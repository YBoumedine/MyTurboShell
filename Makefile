CC = gcc
SOURCEDIR = src
BINDIR = bin
CFLAGS = -Wall
SRC = $(wildcard $(SOURCEDIR)/*.c)
PRGS = $(patsubst $(SOURCEDIR)/%.c, $(BINDIR)/%, $(SRC))
OBJS = $(patsubst $(SOURCEDIR)/%.c,%.o, $(SRC))
MKDIR_P = mkdir -p


.PHONY : all clean dir

all : dir  $(PRGS)

$(BINDIR)/% : %.o
	$(CC) $(CFLAGS) $< -o $@

$(OBJS): $(SRC)
	$(CC) $(CFLAGS) -c $(SRC) $<

dir : ${BINDIR}

${BINDIR} :
		${MKDIR_P} ${BINDIR}

clean:
	rm -f $(OBJS) $(PRGS)
