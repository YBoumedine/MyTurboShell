#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "fin.h"

int lineCounter(char *argv[]){

  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  int count = 0;

  fp = fopen(argv[2], "r");
  if(fp != NULL){

    while((read = getline(&line, &len, fp)) != -1){
      count = count + 1;
    }
  }

  fclose(fp);

  return count;
}

int main(int argc, char *argv[]){

  if(argv[1] != NULL && argv[2] != NULL){

    int lineCount = lineCounter(argv);
    int count = 0, diff = 0, lines = 0;

    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen(argv[2], "r");

    if(fp == NULL){

      fprintf(stderr, "The file doesn't exist.\n");

    }else{

    lines = strtol(argv[1], NULL, 10);
    diff = lineCount - lines - 1;

    if(diff > 0){
      while((read = getline(&line, &len, fp)) != -1 && count != diff){
        count = count + 1;
      }
    }

    while((read = getline(&line, &len, fp)) != -1 ){
      printf("%s", line);
    }

    fclose(fp);

    }
    return 0;

  }else{
    fprintf(stderr, "Invalide argument(s).\n");
    return 1;
  }
}
