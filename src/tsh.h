#ifndef SIZE_H
#define SIZE_H

/**
 * This function parse your commands.
 *
 *
 * @param command, argv
 */
void parseLine(char * command, char **argv);

/**
 * This function execute the function called.
 *
 *
 * @param argv
 */
void executeProgram(char **argv);

/**
 * This function search if the function called exist.
 *
 *
 * @param argv
 */
bool searchProgram(char **argv);

#endif
