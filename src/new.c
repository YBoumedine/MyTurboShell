#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "new.h"

int fileExist(char *file){
  struct stat buffer;
  return (stat(file, &buffer)==0);
}

int main(int argc, char *argv[]){
  FILE *file;

  if(argv[1]== NULL || fileExist(argv[1])){
    printf("Unable to create file.\n");
    return 1;
  }else{
    file = fopen(argv[1], "w+");
    printf("File created.\n");
    fclose(file);
    return 0;
  }

}
