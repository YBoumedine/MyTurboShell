#ifndef RMALL_H
#define RMALL_H

/**
 * This function verifies the access right of the user before deleting.
 *
 * @param dirName
 */
bool checkAccess(char *dirName);

/**
 * This function delete what was specify.
 *
 * @param dirName
 */
void deleteAll(char *dirName);

#endif
