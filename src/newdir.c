#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[]){
  if(mkdir(argv[1], 0700) < 0){
    fprintf(stderr, "Unable to create directory.\n");
    return 1;
  }else{
    printf("Directory created.\n");
    return 0;
  }

}
