#ifndef SIZE_H
#define SIZE_H

/**
 * This function measures the size and the number of files
 * the specify directory has.
 *
 * @param rep
 */
void getInfo(char * rep);

#endif
