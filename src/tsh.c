#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/stat.h>
#include <limits.h>
#include "tsh.h"


bool searchProgram(char **argv){
  char actualPath[PATH_MAX + 1];
  char *home;
  char *ptr;

  home = getenv("HOME");

  sprintf(actualPath, "%s/MyTurboShell/bin/%s", home, argv[0]);

  ptr = realpath(actualPath, NULL);

  if(ptr != NULL){
    free(ptr);
    return true;
  }else{
    perror("errno ");
    free(ptr);
    return false;
  }
}

void currentDir(){
  char cwd[1024];

  if(getcwd(cwd, sizeof(cwd)) != NULL){
    printf("Current directory : %s\n", cwd);
  }else{
    fprintf(stderr, "Error getcwd.. Exiting.\n");
    exit(1);
  }
}

void changeDir(char **argv){
  if(argv[1] == NULL){
    if(chdir(getenv("HOME")) != 0){
      perror("Error in changeDir.. Exiting.\n");
    }
  }else if(chdir(argv[1]) != 0){
    perror("Error in changeDir.. Exiting.\n");
  }

}
void parseLine(char *command, char **argv){

  char *delim = " \n\t";

  char *token = strtok(command, delim);

  while(token != NULL){
    *argv++ = token;
    token = strtok(NULL, delim);
  }
}

void executeProgram(char **argv){

  pid_t childPid;
  int status;
  char actualPath[PATH_MAX+1];
  char *ptr;
  char *home;

  home = getenv("HOME");

  sprintf(actualPath, "%s/MyTurboShell/bin/%s", home, argv[0]);

  ptr = realpath(actualPath, NULL);

  childPid = fork();
  if(childPid < 0 ){

    printf("Fork error.. Exiting.\n");
    exit(1);
  }

  else if(childPid == 0){
    if(execv(ptr, argv) < 0){
      printf("execvp error.. Exiting.\n");
      free(ptr);
      exit(1);
    }
  }

  else{
    while(wait(&status) != childPid);
  }

  free(ptr);
}


int main(void){
  int exit = 0;
  char command[1024];

  while(!exit){
    printf("tsh> ");

    if(fgets(command, sizeof(command), stdin) != NULL){
      char *argv[64] = {NULL};

      parseLine(command, argv);

      if(argv[0] != NULL){

        if(strncmp(argv[0], "exit", 4) == 0){
          exit = 1;
        }else if(strncmp(argv[0], "cdir", 4) == 0){
          currentDir();
        }else if (strncmp(argv[0], "cd", 2) == 0){
          changeDir(argv);
        }else if (searchProgram(argv)){
          executeProgram(argv);
        }else{
          fprintf(stderr, "The program does not exist.\n");
        }
      }
    }
  }
  return 0;
}
