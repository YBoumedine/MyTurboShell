#ifndef FIN_H
#define FIN_H

/**
 * This function counts the number of lines in the file to read.
 *
 * @param argv      reads the file to count the lines.
 */
int lineCounter(char *argv[]);

#endif
