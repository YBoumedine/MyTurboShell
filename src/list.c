#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>

int main(int argc, char *argv[]){

char *cwd;
char P[PATH_MAX+1];
DIR *d;
struct dirent *dir;
d = opendir(".");
cwd = getcwd(P, PATH_MAX+1);

  if(argv[1] != NULL && strcmp(argv[1], "-d") == 0){
    printf("Sous-répertoires de %s\n", cwd);
    if(d){
      while((dir = readdir(d)) != NULL){
        if(dir->d_type & DT_DIR){
          if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
            printf("%s\n", dir->d_name);
          }
        }
      }
      closedir(d);
    }
  }else if(argv[1] == NULL){
    printf("Fichier de %s\n", cwd);
    if(d){
      while((dir = readdir(d)) != NULL){
        if(dir->d_type & DT_REG){
          printf("%s\n", dir->d_name);
        }
      }
      closedir(d);
    }
    return 0;
  }else{
    printf("Unkown command.\n");
  }

  return 1;

}
