#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <limits.h>
#include "rmall.h"


bool checkAccess(char *dirName){
  DIR *directory;
  struct dirent *entry;
  struct stat st;
  char fileName[PATH_MAX];

  if(stat(dirName, &st) < 0) return false;
  if(S_ISREG(st.st_mode)){
    fprintf(stderr, "The file you specified : %s is not a directory\n", dirName );
    return false;
  }

  directory = opendir(dirName);

  while((entry = readdir(directory)) != NULL){

    if ((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))
        continue;

    sprintf(fileName, "%s/%s", dirName, entry->d_name);

    if(stat(fileName, &st) <0) continue;

    if((st.st_mode & S_IWUSR) == 0){
      fprintf(stderr, "You do not have the rights to delete : %s\n", fileName);
      return false;
    }

    if(S_ISDIR(st.st_mode)){
      if(!(checkAccess(fileName))){
        return false;
      }
    }
  }

  return true;

}


void deleteAll(char *dirName){
  DIR *directory;
  struct dirent *entry;
  struct stat st;
  char fileName[PATH_MAX];

  directory = opendir(dirName);

  while((entry = readdir(directory)) != NULL){
    if ((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))
        continue;

    sprintf(fileName, "%s/%s", dirName, entry->d_name);

    if(stat(fileName, &st) <0) continue;

    if(S_ISDIR(st.st_mode)){
      deleteAll(fileName);
    }else{
      remove(fileName);
    }

  }
  remove(dirName);
}


int main(int argc, char *argv[]){

  if(checkAccess(argv[1])){
    deleteAll(argv[1]);
    return 0;
  }else{
    printf("Invalid input.\n");
    return 1;
  }
}
