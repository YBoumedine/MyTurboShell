#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>
#include "size.h"

static int fichiers = 0;
static int octets = 0;

void getInfo(char * rep){
  struct stat buffer;
  DIR *d;
  struct dirent *dir;
  char p[PATH_MAX];

  d = opendir(rep);
  if(d == NULL){

    fprintf(stderr, "The directory doesn't exist.\n");
    exit(1);

  }else{
    while((dir = readdir(d)) != NULL){
      if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){

        sprintf(p, "%s/%s", rep, dir->d_name);

        if(dir->d_type & DT_DIR){
          stat(p, &buffer);
          getInfo(p);
        }else{
          stat(p,&buffer);
          octets += buffer.st_size;
          fichiers ++;
        }

      }
    }
  }
  closedir(d);
}

int main(int argc, char *argv[]){
  char *cwd;
  char path[PATH_MAX];
  cwd = getcwd(path, PATH_MAX);

  if(argv[1] != NULL){
    getInfo(argv[1]);
    printf("Le répertoire %s contient %d fichiers pour un total de %d octets.\n", cwd, fichiers, octets);
    return 0;
  }else{
    printf("Répertoire introuvable.\n");
    return 1;
  }
}
