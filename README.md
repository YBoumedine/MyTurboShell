# Travail pratique 2

## Description

Ce projet à été conçu dans le cadre du cours de système d'exploitation INF3172 pour l'UQÀM. Le logiciel se résume à créer un shell avec quelques fonctionnalités. Il ne s'agit pas d'une simulation, les commandes de notre shell ont un réel impact sur la machine.

## Auteurs

- Youva Boumedine (BOUY19020606)
- Hugues Arly Chanoine (CHAH26108905)

## Plateformes supportées

Les plateformes suivantes sont supportées et testées par notre projet:

  	- MacOS 10.12.1 Sierra
  	- Linux Ubuntu 16.04 LTS

## Fonctionnement
Pour utiliser notre programme, il vous suffit d'extraire le dossier MyTurboShell dans l'environnement HOME de votre machine.

Pour utiliser les fonctionnalités, il suffit d'écrire le nom de la fonction ainsi que les arguments nécessaires pour la lancer. Voir la section Exemple plus basse pour voir les fonctionnalités incluses dans ce projet.



## Installation

Maintenant que vous êtes dans le bon répertoire. Il vous suffit, Pour rendre le projet fonctionnel, de suivre les étapes suivantes toutes en restant dans le répertoire MyTurboShell:

      make

Ensuite, Pour nettoyer le dossier:

      make clean

Pour lancer le programme, il faut lancer l'éxécutable tsh qui se trouve dans le dossier bin de MyTurboShell. Il est important de noter qu'il est possible de le lancer de n'importe où.

## Exemple

Notre shell n’est pas aussi complet que bash, il est limité par aux fonctionnalités qu’il possède. Se sont les suivant :

- cd
- cdir
- exit
- new
- list <-d>
- rmall <directory>
- newdir <directory>
- size <directory>
- fin <number of lines> <file>


-----------------------

###### Premier Exemple:

list -d

Sous-répertoires de MyTurboShell/
bin
src
.git

-----------------------
###### Deuxième Exemple:

size .

Le répertoire MyTurboShell/ contient 15 fichiers pour un total de 20 000 octets.


-----------------------

## contenu du TP

 - MyTurboShell
  - src
     - fin.c
     - fin.h
     - list.c
     - new.c
     - new.h
     - newdir.c
     - rmall.c
     - rmall.h
     - size.c
     - size.h
     - tsh.c
     - tsh.h
  - MakeFile
  - README.md      

## Références

Voici les sites que nous avons utilisés pour faire le projet :

- http://stackoverflow.com/

- https://www.tutorialspoint.com/

- http://pubs.opengroup.org/onlinepubs/7908799/

- http://www.jberger.org/inf3172/


## Statut


##### En conclusion, Le projet est complet. Tous les fonctionnalités ont été implantées. Le projet a été testé sur IOS, Linux et Malt et tous semble se dérouler comme il se doit.
